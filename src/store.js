import { createStore, combineReducers } from 'redux'
import { counter } from './reducers/counter.reducer'
import { todos, initialTodos } from './reducers/todos.reducer'
import { users, initialUsers } from './reducers/users.reducer'

const initialState = {
    value: 0,
    nested: {
        counter: 1
    },
    todos: initialTodos,
    users: initialUsers
}


const rootReducer = combineReducers({
    value: counter,
    nested: combineReducers({
        counter
    }),
    todos,
    users
})

export const store = createStore(rootReducer, initialState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)


store.subscribe(() => {
    console.log('STATE:', store.getState())
})

window.store = store;