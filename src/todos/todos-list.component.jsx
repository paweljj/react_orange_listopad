import React from 'react'

export class TodosList extends React.PureComponent {

    componentWillReceiveProps( newProps = {} ){
        // console.log(this.props, newProps)
        if(this.props.items != newProps.items){
            this.completed = this.getCompleted(newProps.items)
        }
    }

    // shouldComponentUpdate( newProps={}, newState={} ){
    //     return this.props.items != newProps.items
    // }
    
    completed = 0

    getCompleted(todos){
        return todos.reduce( (count, todo) => count += todo.completed , 0)
    }

    render() {
        let { items = [], onToggle, onRemove } = this.props;

        return <div>
            <div className="list-group">
                {items.map((item, index) =>
                    <div className="list-group-item" key={item.id}>
                        <input type="checkbox"
                            checked={item.completed}
                            onChange={e => onToggle(item, e)}
                        />
                        {' '} {index + 1}. {item.title}
                        <span className="close" onClick={e => onRemove(item)}>&times;</span>
                    </div>
                )}
            </div>
            <p>Completed: {this.completed}</p>
        </div>
    }
}