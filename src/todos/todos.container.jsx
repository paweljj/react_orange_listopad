import React from 'react'
import { TodosList } from '../todos/todos-list.component'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { todoAdd, todoRemove, todoFromTitle, todoToggle} from '../reducers/todos.reducer'

class TodosContainer extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            newTodoTitle: ''
        }
    }

    onUpdate = (event) => {
        this.setState({
            newTodoTitle: event.target.value
        })
    }

    addOnEnter = (event) => {
        if (event.keyCode == 13 && this.state.newTodoTitle) {
            
            this.props.todoFromTitle(this.state.newTodoTitle)

            this.setState(() => ({
                newTodoTitle: ''
            }))
        }
    }


    removeTodo = (todo) => {
        this.props.todoRemove(todo.id)
    }

    componentWillUpdate( ){
        // Wykonuje sie po wszystkich setState()
        // this.completedCache = calculateCompleted()
    }

    componentDidMount( ){
        this.input.focus()

        // this.input.insertAdjacentHTML('beforebegin','<b>Test</b>')
    }
    
    componentDidUpdate( ){

    }

    render() {
        let { color = 'black', title } = this.props

        return <div>
            <h3 style={{ color }}>{title}</h3>

            <TodosList
                items={this.props.todos}
                onToggle={todo => this.props.todoToggle(todo)}
                onRemove={this.removeTodo}
            />

            <hr />
            <input className="form-control"
                ref={ elem => this.input = elem }
                onChange={this.onUpdate}
                onKeyUp={this.addOnEnter}
                value={this.state.newTodoTitle} />

            {this.state.newTodoTitle.length}
        </div>
    }
}

export const Todos = connect(
    // mapStateToProps
    state => ({
        todos: state.todos.list
    }),
    // mapDispatchToProps
    dispatch => bindActionCreators({
        todoAdd,
        todoRemove,
        todoToggle,
        todoFromTitle,
    },dispatch),
    // Merge Prosp with OwnProps
    // (stateProps, distpachProps, ownProps) => {

    // }
)(TodosContainer)