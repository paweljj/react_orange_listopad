import React from 'react'
import { UsersList } from './users-list.component'
import { UserForm } from './user-form.component'
import { connect } from 'react-redux'
import {
  Route,
  Redirect,
  NavLink,
  Link
} from 'react-router-dom'

import { fetchUsers, saveUser } from '../reducers/users.reducer'


const Form = connect(
    state => ({ 
        users: state.users.list 
    }),
    dispatch => ({
        saveUser: saveUser(dispatch)
    }),
    (stateProps, dispatchProps, ownProps) => {

        // dispatchProps.fetchUser(ownProps.id)

        // console.log(stateProps.users, ownProps.id)
        return Object.assign({},stateProps,dispatchProps,{
            user: stateProps.users.find( user => user.id == ownProps.id )
        })
    }
)(UserForm)



class UsersContainer extends React.Component {

    state = {
        selected: null
    }

    componentDidMount() {
        this.getUsers()
    }

    componentWillUnmount() {
        console.log('componentWillUnmount')
    }

    getUsers = () => {
        this.props.fetchUsers()
        .then(()=>{
            console.log('fetched')
        })
    }

    selectUser = (selected) => {
        this.props.history.push('/users/'+selected.id)

        this.setState({
            selected
        })
    }

    saveUser = (user) => {
        this.props.saveUser(user)
        .then((user)=>{
            this.selectUser(user)
        })
     
    }

    render() {
        return <div>
            <div className="row">
                <div className="col">

                    <UsersList users={this.props.users}
                        selected={this.state.selected}
                        onSelect={this.selectUser} />

                    <button onClick={this.getUsers}>Update</button>
                    <button onClick={()=>this.selectUser({})}>Create New</button>

                </div>
                <div className="col">
                   <Route path="/users/:id" render={ (props) => <div>
                        {props.match.params.id} 
                        <Form id={props.match.params.id} />
                    </div>} />

                </div>
            </div>
        </div>
    }
}

export const Users = connect(
    state => ({
        users: state.users.list
    }),
    dispatch => ({
        fetchUsers: () => fetchUsers(dispatch),
        saveUser: saveUser(dispatch)
    })
)(UsersContainer)