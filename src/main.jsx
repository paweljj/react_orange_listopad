import style from './style.css'

// React: https://reactjs.org/
import React from 'react'
import { render } from 'react-dom'

// Redux: https://redux.js.org/docs/introduction/
import { store } from './store'
import { Provider } from 'react-redux'

// Router: https://reacttraining.com/react-router/web/guides/quick-start
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

// App:
import { App } from './app/app.container'

render(
<Router>
    <Provider store={store}>
        <App />
    </Provider>
</Router>
, document.getElementById('app-root'))
