import React from 'react'
import { Todos } from '../todos/todos.container'
import { Users } from '../users/users.container'

import {
  Route,
  Redirect,
  NavLink,
  Link
} from 'react-router-dom'

const todos = [
    { id: 1, title: 'Zakupy', completed: true },
    { id: 2, title: 'Placki', completed: true },
    { id: 3, title: 'React!', completed: false },
    { id: 4, title: 'Profit!', completed: false },
]

export class App extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            tab: 'users',
            todos: todos,
            newTodoTitle: ''
        }
    }

    setTab = (tab = 'users') => {
        this.setState({
            tab
        })
    }

    render() {
        return <div className="container">

            <div className="row">
                <div className="col">
                    <ul className="nav nav-pills">
                        <li className="nav-item">
                            <NavLink to="/" activeClassName="active" className="nav-link">Users</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/todos" activeClassName="active" className="nav-link">Todos</NavLink>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="row">
                <div className="col">
                    
                    <Route path="/" exact render={()=> 
                        <Redirect to="/users" /> 
                    } />

                    <Route path="/users" component={Users} />
                    <Route path="/todos" component={Todos} />
                </div>
            </div>
        </div>
    }
}


